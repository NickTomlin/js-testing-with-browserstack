Testem + Mocha + Chai + Browserstack
---

Cross browser testing.

Deps:

    npm install -g testem browserstack-cli grunt-cli
    npm install
    # have browserstack username/password handy!
    browserstack setup

Development:

    grunt
