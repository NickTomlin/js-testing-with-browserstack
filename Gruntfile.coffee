grunt.initConfig testem:
  environment1:

    # List of files to attach
    src: ["bower_components/jquery/jquery.js", "source/**/*.coffee", "spec/helpers/*.coffee", "spec/**/*_spec.coffee"]

    # Options that will be passed to Testem
    options:
      framework: "mocha"
       src_files: ["src/*", "test/*"]
       launchers:
         bs_opera:
           command: "browserstack launch --attach opera:12.11 localhost:7357"
           protocol: "browser"

         bs_firefox:
           command: "browserstack launch --attach firefox:16.0 localhost:7357"
           protocol: "browser"

         bs_chrome:
           command: "browserstack launch --attach chrome:24.0 localhost:7357"
           protocol: "browser"

         bs_safari_5:
           command: "browserstack launch --attach safari:5.0 localhost:7357"
           protocol: "browser"

         bs_safari_51:
           command: "browserstack launch --attach safari:5.1 localhost:7357"
           protocol: "browser"

         bs_ie_7:
           command: "browserstack launch --attach ie:7.0 localhost:7357"
           protocol: "browser"

         bs_ie_8:
           command: "browserstack launch --attach ie:8.0 localhost:7357"
           protocol: "browser"

         bs_ie_9:
           command: "browserstack launch --attach ie:9.0 localhost:7357"
           protocol: "browser"

       launch_in_ci: ["bs_opera", "bs_chrome", "bs_firefox", "bs_safari_51", "bs_safari_5", "bs_ie_8", "bs_ie_9", "bs_ie_7"]
