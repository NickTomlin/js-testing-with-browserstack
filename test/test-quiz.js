var expect = chai.expect;

describe("A Quiz", function () {
  it("should have a default score of 0", function () {
    var quiz = new Quiz;
    expect(quiz.score).to.equal(0);
  })
  it("should have a title", function () {
    var quiz = new Quiz('my great quiz');
    expect(quiz.title).to.be.ok;
  })
});
